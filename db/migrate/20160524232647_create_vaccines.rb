class CreateVaccines < ActiveRecord::Migration
  def change
    create_table :vaccines do |t|
      t.references :pet, index: true, foreign_key: true
      t.string :veterinary_name
      t.integer :parent_vaccine
      t.text :notes
      t.date :date

      t.timestamps null: false
    end
  end
end
