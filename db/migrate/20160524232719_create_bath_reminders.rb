class CreateBathReminders < ActiveRecord::Migration
  def change
    create_table :bath_reminders do |t|
      t.references :bath, index: true, foreign_key: true
      t.string :title
      t.date :date

      t.timestamps null: false
    end
  end
end
