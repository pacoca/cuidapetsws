class CreateBaths < ActiveRecord::Migration
  def change
    create_table :baths do |t|
      t.string :title
      t.date :date
      t.text :notes
      t.decimal :price, precision: 6, scale: 2

      t.timestamps null: false
    end
  end
end
