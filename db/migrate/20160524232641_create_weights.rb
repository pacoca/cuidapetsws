class CreateWeights < ActiveRecord::Migration
  def change
    create_table :weights do |t|
      t.references :pet, index: true, foreign_key: true
      t.decimal :value, precision: 6, scale: 3
      t.date :date

      t.timestamps null: false
    end
  end
end
