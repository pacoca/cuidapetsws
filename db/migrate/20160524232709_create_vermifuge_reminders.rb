class CreateVermifugeReminders < ActiveRecord::Migration
  def change
    create_table :vermifuge_reminders do |t|
      t.references :vermifuge, index: true, foreign_key: true
      t.date :date

      t.timestamps null: false
    end
  end
end
