class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.references :pet, index: true, foreign_key: true
      t.string :title
      t.text :notes
      t.date :date

      t.timestamps null: false
    end
  end
end
