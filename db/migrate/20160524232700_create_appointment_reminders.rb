class CreateAppointmentReminders < ActiveRecord::Migration
  def change
    create_table :appointment_reminders do |t|
      t.references :appointment, index: true, foreign_key: true
      t.string :title
      t.date :date

      t.timestamps null: false
    end
  end
end
