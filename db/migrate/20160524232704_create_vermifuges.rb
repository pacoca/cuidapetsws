class CreateVermifuges < ActiveRecord::Migration
  def change
    create_table :vermifuges do |t|
      t.string :title
      t.string :manufacturer_name
      t.date :date
      t.string :dose, limit: 25
      t.text :notes
      t.string :image

      t.timestamps null: false
    end
  end
end
