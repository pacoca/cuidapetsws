class CreateOwners < ActiveRecord::Migration
  def change
    create_table :owners do |t|
      t.string :name
      t.string :email
      t.date :birth
      t.string :username
      t.string :image
      t.string :phone

      t.timestamps null: false
    end
    add_index :owners, :email
    add_index :owners, :username
  end
end
