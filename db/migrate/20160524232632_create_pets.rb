class CreatePets < ActiveRecord::Migration
  def change
    create_table :pets do |t|
      t.references :owner, index: true, foreign_key: true
      t.string :name
      t.date :birth
      t.string :sex, limit: 2
      t.string :specie
      t.string :breed
      t.string :color
      t.string :image

      t.timestamps null: false
    end
  end
end
