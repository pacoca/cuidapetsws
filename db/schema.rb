# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160524232719) do

  create_table "appointment_reminders", force: :cascade do |t|
    t.integer  "appointment_id", limit: 4
    t.string   "title",          limit: 255
    t.date     "date"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "appointment_reminders", ["appointment_id"], name: "index_appointment_reminders_on_appointment_id", using: :btree

  create_table "appointments", force: :cascade do |t|
    t.integer  "pet_id",     limit: 4
    t.string   "title",      limit: 255
    t.text     "notes",      limit: 65535
    t.date     "date"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "appointments", ["pet_id"], name: "index_appointments_on_pet_id", using: :btree

  create_table "bath_reminders", force: :cascade do |t|
    t.integer  "bath_id",    limit: 4
    t.string   "title",      limit: 255
    t.date     "date"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "bath_reminders", ["bath_id"], name: "index_bath_reminders_on_bath_id", using: :btree

  create_table "baths", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.date     "date"
    t.text     "notes",      limit: 65535
    t.decimal  "price",                    precision: 6, scale: 2
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  create_table "owners", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "email",      limit: 255
    t.date     "birth"
    t.string   "username",   limit: 255
    t.string   "image",      limit: 255
    t.string   "phone",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "owners", ["email"], name: "index_owners_on_email", using: :btree
  add_index "owners", ["username"], name: "index_owners_on_username", using: :btree

  create_table "pets", force: :cascade do |t|
    t.integer  "owner_id",   limit: 4
    t.string   "name",       limit: 255
    t.date     "birth"
    t.string   "sex",        limit: 2
    t.string   "specie",     limit: 255
    t.string   "breed",      limit: 255
    t.string   "color",      limit: 255
    t.string   "image",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "pets", ["owner_id"], name: "index_pets_on_owner_id", using: :btree

  create_table "vaccines", force: :cascade do |t|
    t.integer  "pet_id",          limit: 4
    t.string   "veterinary_name", limit: 255
    t.integer  "parent_vaccine",  limit: 4
    t.text     "notes",           limit: 65535
    t.date     "date"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "vaccines", ["pet_id"], name: "index_vaccines_on_pet_id", using: :btree

  create_table "vermifuge_reminders", force: :cascade do |t|
    t.integer  "vermifuge_id", limit: 4
    t.date     "date"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "vermifuge_reminders", ["vermifuge_id"], name: "index_vermifuge_reminders_on_vermifuge_id", using: :btree

  create_table "vermifuges", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.string   "manufacturer_name", limit: 255
    t.date     "date"
    t.string   "dose",              limit: 25
    t.text     "notes",             limit: 65535
    t.string   "image",             limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "weights", force: :cascade do |t|
    t.integer  "pet_id",     limit: 4
    t.decimal  "value",                precision: 6, scale: 3
    t.date     "date"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "weights", ["pet_id"], name: "index_weights_on_pet_id", using: :btree

  add_foreign_key "appointment_reminders", "appointments"
  add_foreign_key "appointments", "pets"
  add_foreign_key "bath_reminders", "baths"
  add_foreign_key "pets", "owners"
  add_foreign_key "vaccines", "pets"
  add_foreign_key "vermifuge_reminders", "vermifuges"
  add_foreign_key "weights", "pets"
end
