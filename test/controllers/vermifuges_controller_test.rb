require 'test_helper'

class VermifugesControllerTest < ActionController::TestCase
  setup do
    @vermifuge = vermifuges(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vermifuges)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vermifuge" do
    assert_difference('Vermifuge.count') do
      post :create, vermifuge: { date: @vermifuge.date, dose: @vermifuge.dose, image: @vermifuge.image, manufacturer_name: @vermifuge.manufacturer_name, notes: @vermifuge.notes, title: @vermifuge.title }
    end

    assert_redirected_to vermifuge_path(assigns(:vermifuge))
  end

  test "should show vermifuge" do
    get :show, id: @vermifuge
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vermifuge
    assert_response :success
  end

  test "should update vermifuge" do
    patch :update, id: @vermifuge, vermifuge: { date: @vermifuge.date, dose: @vermifuge.dose, image: @vermifuge.image, manufacturer_name: @vermifuge.manufacturer_name, notes: @vermifuge.notes, title: @vermifuge.title }
    assert_redirected_to vermifuge_path(assigns(:vermifuge))
  end

  test "should destroy vermifuge" do
    assert_difference('Vermifuge.count', -1) do
      delete :destroy, id: @vermifuge
    end

    assert_redirected_to vermifuges_path
  end
end
