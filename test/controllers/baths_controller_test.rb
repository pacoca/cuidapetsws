require 'test_helper'

class BathsControllerTest < ActionController::TestCase
  setup do
    @bath = baths(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:baths)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bath" do
    assert_difference('Bath.count') do
      post :create, bath: { date: @bath.date, notes: @bath.notes, price: @bath.price, title: @bath.title }
    end

    assert_redirected_to bath_path(assigns(:bath))
  end

  test "should show bath" do
    get :show, id: @bath
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bath
    assert_response :success
  end

  test "should update bath" do
    patch :update, id: @bath, bath: { date: @bath.date, notes: @bath.notes, price: @bath.price, title: @bath.title }
    assert_redirected_to bath_path(assigns(:bath))
  end

  test "should destroy bath" do
    assert_difference('Bath.count', -1) do
      delete :destroy, id: @bath
    end

    assert_redirected_to baths_path
  end
end
