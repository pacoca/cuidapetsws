require 'test_helper'

class BathRemindersControllerTest < ActionController::TestCase
  setup do
    @bath_reminder = bath_reminders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bath_reminders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bath_reminder" do
    assert_difference('BathReminder.count') do
      post :create, bath_reminder: { bath_id: @bath_reminder.bath_id, date: @bath_reminder.date, title: @bath_reminder.title }
    end

    assert_redirected_to bath_reminder_path(assigns(:bath_reminder))
  end

  test "should show bath_reminder" do
    get :show, id: @bath_reminder
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bath_reminder
    assert_response :success
  end

  test "should update bath_reminder" do
    patch :update, id: @bath_reminder, bath_reminder: { bath_id: @bath_reminder.bath_id, date: @bath_reminder.date, title: @bath_reminder.title }
    assert_redirected_to bath_reminder_path(assigns(:bath_reminder))
  end

  test "should destroy bath_reminder" do
    assert_difference('BathReminder.count', -1) do
      delete :destroy, id: @bath_reminder
    end

    assert_redirected_to bath_reminders_path
  end
end
