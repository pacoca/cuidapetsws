require 'test_helper'

class AppointmentRemindersControllerTest < ActionController::TestCase
  setup do
    @appointment_reminder = appointment_reminders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:appointment_reminders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create appointment_reminder" do
    assert_difference('AppointmentReminder.count') do
      post :create, appointment_reminder: { appointment_id: @appointment_reminder.appointment_id, date: @appointment_reminder.date, title: @appointment_reminder.title }
    end

    assert_redirected_to appointment_reminder_path(assigns(:appointment_reminder))
  end

  test "should show appointment_reminder" do
    get :show, id: @appointment_reminder
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @appointment_reminder
    assert_response :success
  end

  test "should update appointment_reminder" do
    patch :update, id: @appointment_reminder, appointment_reminder: { appointment_id: @appointment_reminder.appointment_id, date: @appointment_reminder.date, title: @appointment_reminder.title }
    assert_redirected_to appointment_reminder_path(assigns(:appointment_reminder))
  end

  test "should destroy appointment_reminder" do
    assert_difference('AppointmentReminder.count', -1) do
      delete :destroy, id: @appointment_reminder
    end

    assert_redirected_to appointment_reminders_path
  end
end
