require 'test_helper'

class VermifugeRemindersControllerTest < ActionController::TestCase
  setup do
    @vermifuge_reminder = vermifuge_reminders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:vermifuge_reminders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create vermifuge_reminder" do
    assert_difference('VermifugeReminder.count') do
      post :create, vermifuge_reminder: { date: @vermifuge_reminder.date, vermifuge_id: @vermifuge_reminder.vermifuge_id }
    end

    assert_redirected_to vermifuge_reminder_path(assigns(:vermifuge_reminder))
  end

  test "should show vermifuge_reminder" do
    get :show, id: @vermifuge_reminder
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @vermifuge_reminder
    assert_response :success
  end

  test "should update vermifuge_reminder" do
    patch :update, id: @vermifuge_reminder, vermifuge_reminder: { date: @vermifuge_reminder.date, vermifuge_id: @vermifuge_reminder.vermifuge_id }
    assert_redirected_to vermifuge_reminder_path(assigns(:vermifuge_reminder))
  end

  test "should destroy vermifuge_reminder" do
    assert_difference('VermifugeReminder.count', -1) do
      delete :destroy, id: @vermifuge_reminder
    end

    assert_redirected_to vermifuge_reminders_path
  end
end
