json.array!(@vermifuge_reminders) do |vermifuge_reminder|
  json.extract! vermifuge_reminder, :id, :vermifuge_id, :date
  json.url vermifuge_reminder_url(vermifuge_reminder, format: :json)
end
