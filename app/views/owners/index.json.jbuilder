json.array!(@owners) do |owner|
  json.extract! owner, :id, :name, :email, :birth, :username, :image, :phone
  json.url owner_url(owner, format: :json)
end
