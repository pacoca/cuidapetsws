json.array!(@vermifuges) do |vermifuge|
  json.extract! vermifuge, :id, :title, :manufacturer_name, :date, :dose, :notes, :image
  json.url vermifuge_url(vermifuge, format: :json)
end
