json.array!(@bath_reminders) do |bath_reminder|
  json.extract! bath_reminder, :id, :bath_id, :title, :date
  json.url bath_reminder_url(bath_reminder, format: :json)
end
