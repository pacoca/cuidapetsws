json.array!(@pets) do |pet|
  json.extract! pet, :id, :owner_id, :name, :birth, :sex, :specie, :breed, :color, :image
  json.url pet_url(pet, format: :json)
end
