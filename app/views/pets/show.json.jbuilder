json.extract! @pet, :id, :owner_id, :name, :birth, :sex, :specie, :breed, :color, :image, :created_at, :updated_at
