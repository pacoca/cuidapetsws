json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :pet_id, :title, :notes, :date
  json.url appointment_url(appointment, format: :json)
end
