json.array!(@vaccines) do |vaccine|
  json.extract! vaccine, :id, :pet_id, :veterinary_name, :parent_vaccine, :notes, :date
  json.url vaccine_url(vaccine, format: :json)
end
