json.array!(@appointment_reminders) do |appointment_reminder|
  json.extract! appointment_reminder, :id, :appointment_id, :title, :date
  json.url appointment_reminder_url(appointment_reminder, format: :json)
end
