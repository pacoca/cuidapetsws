json.array!(@baths) do |bath|
  json.extract! bath, :id, :title, :date, :notes, :price
  json.url bath_url(bath, format: :json)
end
