class AppointmentRemindersController < ApplicationController
  before_action :set_appointment_reminder, only: [:show, :edit, :update, :destroy]

  # GET /appointment_reminders
  # GET /appointment_reminders.json
  def index
    @appointment_reminders = AppointmentReminder.all
  end

  # GET /appointment_reminders/1
  # GET /appointment_reminders/1.json
  def show
  end

  # GET /appointment_reminders/new
  def new
    @appointment_reminder = AppointmentReminder.new
  end

  # GET /appointment_reminders/1/edit
  def edit
  end

  # POST /appointment_reminders
  # POST /appointment_reminders.json
  def create
    @appointment_reminder = AppointmentReminder.new(appointment_reminder_params)

    respond_to do |format|
      if @appointment_reminder.save
        format.html { redirect_to @appointment_reminder, notice: 'Appointment reminder was successfully created.' }
        format.json { render :show, status: :created, location: @appointment_reminder }
      else
        format.html { render :new }
        format.json { render json: @appointment_reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appointment_reminders/1
  # PATCH/PUT /appointment_reminders/1.json
  def update
    respond_to do |format|
      if @appointment_reminder.update(appointment_reminder_params)
        format.html { redirect_to @appointment_reminder, notice: 'Appointment reminder was successfully updated.' }
        format.json { render :show, status: :ok, location: @appointment_reminder }
      else
        format.html { render :edit }
        format.json { render json: @appointment_reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /appointment_reminders/1
  # DELETE /appointment_reminders/1.json
  def destroy
    @appointment_reminder.destroy
    respond_to do |format|
      format.html { redirect_to appointment_reminders_url, notice: 'Appointment reminder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appointment_reminder
      @appointment_reminder = AppointmentReminder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appointment_reminder_params
      params.require(:appointment_reminder).permit(:appointment_id, :title, :date)
    end
end
