class BathRemindersController < ApplicationController
  before_action :set_bath_reminder, only: [:show, :edit, :update, :destroy]

  # GET /bath_reminders
  # GET /bath_reminders.json
  def index
    @bath_reminders = BathReminder.all
  end

  # GET /bath_reminders/1
  # GET /bath_reminders/1.json
  def show
  end

  # GET /bath_reminders/new
  def new
    @bath_reminder = BathReminder.new
  end

  # GET /bath_reminders/1/edit
  def edit
  end

  # POST /bath_reminders
  # POST /bath_reminders.json
  def create
    @bath_reminder = BathReminder.new(bath_reminder_params)

    respond_to do |format|
      if @bath_reminder.save
        format.html { redirect_to @bath_reminder, notice: 'Bath reminder was successfully created.' }
        format.json { render :show, status: :created, location: @bath_reminder }
      else
        format.html { render :new }
        format.json { render json: @bath_reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bath_reminders/1
  # PATCH/PUT /bath_reminders/1.json
  def update
    respond_to do |format|
      if @bath_reminder.update(bath_reminder_params)
        format.html { redirect_to @bath_reminder, notice: 'Bath reminder was successfully updated.' }
        format.json { render :show, status: :ok, location: @bath_reminder }
      else
        format.html { render :edit }
        format.json { render json: @bath_reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bath_reminders/1
  # DELETE /bath_reminders/1.json
  def destroy
    @bath_reminder.destroy
    respond_to do |format|
      format.html { redirect_to bath_reminders_url, notice: 'Bath reminder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bath_reminder
      @bath_reminder = BathReminder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bath_reminder_params
      params.require(:bath_reminder).permit(:bath_id, :title, :date)
    end
end
