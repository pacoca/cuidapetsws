class VermifugeRemindersController < ApplicationController
  before_action :set_vermifuge_reminder, only: [:show, :edit, :update, :destroy]

  # GET /vermifuge_reminders
  # GET /vermifuge_reminders.json
  def index
    @vermifuge_reminders = VermifugeReminder.all
  end

  # GET /vermifuge_reminders/1
  # GET /vermifuge_reminders/1.json
  def show
  end

  # GET /vermifuge_reminders/new
  def new
    @vermifuge_reminder = VermifugeReminder.new
  end

  # GET /vermifuge_reminders/1/edit
  def edit
  end

  # POST /vermifuge_reminders
  # POST /vermifuge_reminders.json
  def create
    @vermifuge_reminder = VermifugeReminder.new(vermifuge_reminder_params)

    respond_to do |format|
      if @vermifuge_reminder.save
        format.html { redirect_to @vermifuge_reminder, notice: 'Vermifuge reminder was successfully created.' }
        format.json { render :show, status: :created, location: @vermifuge_reminder }
      else
        format.html { render :new }
        format.json { render json: @vermifuge_reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vermifuge_reminders/1
  # PATCH/PUT /vermifuge_reminders/1.json
  def update
    respond_to do |format|
      if @vermifuge_reminder.update(vermifuge_reminder_params)
        format.html { redirect_to @vermifuge_reminder, notice: 'Vermifuge reminder was successfully updated.' }
        format.json { render :show, status: :ok, location: @vermifuge_reminder }
      else
        format.html { render :edit }
        format.json { render json: @vermifuge_reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vermifuge_reminders/1
  # DELETE /vermifuge_reminders/1.json
  def destroy
    @vermifuge_reminder.destroy
    respond_to do |format|
      format.html { redirect_to vermifuge_reminders_url, notice: 'Vermifuge reminder was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vermifuge_reminder
      @vermifuge_reminder = VermifugeReminder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vermifuge_reminder_params
      params.require(:vermifuge_reminder).permit(:vermifuge_id, :date)
    end
end
