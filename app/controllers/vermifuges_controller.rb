class VermifugesController < ApplicationController
  before_action :set_vermifuge, only: [:show, :edit, :update, :destroy]

  # GET /vermifuges
  # GET /vermifuges.json
  def index
    @vermifuges = Vermifuge.all
  end

  # GET /vermifuges/1
  # GET /vermifuges/1.json
  def show
  end

  # GET /vermifuges/new
  def new
    @vermifuge = Vermifuge.new
  end

  # GET /vermifuges/1/edit
  def edit
  end

  # POST /vermifuges
  # POST /vermifuges.json
  def create
    @vermifuge = Vermifuge.new(vermifuge_params)

    respond_to do |format|
      if @vermifuge.save
        format.html { redirect_to @vermifuge, notice: 'Vermifuge was successfully created.' }
        format.json { render :show, status: :created, location: @vermifuge }
      else
        format.html { render :new }
        format.json { render json: @vermifuge.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vermifuges/1
  # PATCH/PUT /vermifuges/1.json
  def update
    respond_to do |format|
      if @vermifuge.update(vermifuge_params)
        format.html { redirect_to @vermifuge, notice: 'Vermifuge was successfully updated.' }
        format.json { render :show, status: :ok, location: @vermifuge }
      else
        format.html { render :edit }
        format.json { render json: @vermifuge.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vermifuges/1
  # DELETE /vermifuges/1.json
  def destroy
    @vermifuge.destroy
    respond_to do |format|
      format.html { redirect_to vermifuges_url, notice: 'Vermifuge was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vermifuge
      @vermifuge = Vermifuge.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def vermifuge_params
      params.require(:vermifuge).permit(:title, :manufacturer_name, :date, :dose, :notes, :image)
    end
end
